$(document).ready(function () {

  $('.kp-top-carousel').slick({
    arrows: false,
    autoplay: true
  });

  $('.kp-reviews').slick({
    arrows: true,
    dots: false,
    autoplay: false
  });

  $('.kp-nav-toggle').click(function () {
    $('.kp-nav').slideToggle('slow', function () {
      // Animation complete.
    });
  });

});